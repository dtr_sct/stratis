<!DOCTYPE html>
<html>
<head>
  <#include "header.ftl">
</head>

<body>

  <#include "nav.ftl">

<div class="container">

  <h1>Input</h1>
    <ul>
    <#list v1 as x>
      <li> ${x} </li>
    </#list>
    </ul>

</div>

<div class="container">

  <h1>Output</h1>
    <ul>
    <#list results as x>
      <li> ${x} </li>
    </#list>
    </ul>

</div>

</body>
</html>

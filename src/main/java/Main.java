import java.sql.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.net.URI;
import java.net.URISyntaxException;

import static spark.Spark.*;
import spark.template.freemarker.FreeMarkerEngine;
import spark.ModelAndView;
import static spark.Spark.get;

import com.heroku.sdk.jdbc.DatabaseUrl;

public class Main {

	public static void main(String[] args) {

    port(Integer.valueOf(System.getenv("PORT")));
    staticFileLocation("/public");

    get("/testFunction", (req, res) -> {
        ArrayList<String> output = new ArrayList<String>();
        ArrayList<String> input = new ArrayList<String>();
        Map<String, Object> attributes = new HashMap<>();
        ArrayList<String> result = new ArrayList<String>();
        String arg1 = req.queryParams("arg1");
        String arg2 = req.queryParams("arg2");
        int value = -1;
        input.add( "Read from URL attribute arg1: "+ arg1);
        input.add( "Read from URL attribute arg2: "+ arg2);
        
        String[] words = arg2.split(" ");
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        for (int i = 0; i < words.length; i++) {
			try {
				numbers.add(Integer.parseInt(words[i]));
			} catch (Exception e) {
			}
		}
        
        try {
			value = Integer.parseInt(arg1);
			if (value>=0 && value <= 9) {		        
		        for (Integer num : numbers) {
		        	Set<CharSequence> secuence = greatestNumberOfXLessThanY(value, num);
		        	for (Iterator<CharSequence> iterator = secuence.iterator(); iterator.hasNext();) {
		        		result.add((String) iterator.next());
					}
				}
			}else{
				result.add("arg1 is not a number between 0 and 9");
			}
		} catch (Exception e) {
			result.add("arg1 is not a integer");
		}


        
        for(int i = 0; i < words.length; i++) {
        	Set<CharSequence> palindrome = printAllPalindromes(words[i].toUpperCase());
        	for (Iterator<CharSequence> iterator = palindrome.iterator(); iterator.hasNext();) {
        		result.add((String) iterator.next());
			}
		}

        
        output.add( "Result of the operation: "+ result+" ");    
        
        attributes.put("v1", input);
        attributes.put("results", output);
        return new ModelAndView(attributes, "output.ftl");
    },new FreeMarkerEngine());
    
    get("/hello", (req, res) -> "Hello World");

    get("/", (request, response) -> {
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("message", "Hello World!");

            return new ModelAndView(attributes, "index.ftl");
        }, new FreeMarkerEngine());

    get("/db", (req, res) -> {
      Connection connection = null;
      Map<String, Object> attributes = new HashMap<>();
      try {
        connection = DatabaseUrl.extract().getConnection();

        Statement stmt = connection.createStatement();
        stmt.executeUpdate("CREATE TABLE IF NOT EXISTS ticks (tick timestamp)");
        stmt.executeUpdate("INSERT INTO ticks VALUES (now())");
        ResultSet rs = stmt.executeQuery("SELECT tick FROM ticks");

        ArrayList<String> output = new ArrayList<String>();
        while (rs.next()) {
          output.add( "Read from DB: " + rs.getTimestamp("tick"));
        }

        attributes.put("results", output);
        return new ModelAndView(attributes, "db.ftl");
      } catch (Exception e) {
        attributes.put("message", "There was an error: " + e);
        return new ModelAndView(attributes, "error.ftl");
      } finally {
        if (connection != null) try{connection.close();} catch(SQLException e){}
      }
    }, new FreeMarkerEngine());

  }

	public static Set<CharSequence> printAllPalindromes(String input) {
		if (input.length() <= 2) {
			return Collections.emptySet();
		}
		Set<CharSequence> out = new HashSet<CharSequence>();
		int length = input.length();
		for (int i = 1; i <= length; i++) {
			for (int j = i - 1, k = i; j >= 0 && k < length; j--, k++) {
				if (Character.isLetter(input.charAt(k))
						&& input.charAt(j) == input.charAt(k)) {
					out.clear();
					out.add(input.subSequence(j, k + 1));
				} else {
					break;
				}
			}
		}
		return out;
	}

	public static Set<CharSequence> greatestNumberOfXLessThanY(int X, int Y) {
		String input = "" + Y;
		if (input.length() <= 0) {
			return Collections.emptySet();
		}
		Set<CharSequence> out = new HashSet<CharSequence>();
		int length = input.length();
		for (int i = 0; i < length; i++) {
			CharSequence subNumber = input.subSequence(0, i + 1);
			try {
				int subN = Integer.parseInt((String) subNumber);
				if (subN % X == 0) {
					out.clear();
					out.add("" + subN);
				}
			} catch (Exception e) {
			}

		}
		return out;
	}

}
